package com.demo.sephora_demo;

import com.demo.sephora_demo.api.APIInterface;
import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.demo.sephora_demo.common.Constant.*;
import static com.demo.sephora_demo.common.NetInfo.BASE_URL;

public class UnitTest {

    @Test
    public void checkApiCall() {
        Map<String, Object> params = new HashMap<>();
        params.put(paramKey1,paramVal1);
        params.put(paramKey2,paramVal2);
        params.put(paramKey3,paramVal3);
        params.put(paramKey4,paramVal4);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder()
                        .addHeader(ACCEPT,ACCEPT_VAL)
                        .addHeader(XDMODEL,XDMODEL_VAL)
                        .addHeader(XPLATFORM,XPLATFORM_VAL)
                        .addHeader(USERAGENT,USERAGENT_VAL)
                        .addHeader(XAVERSION,XAVERSION_VAL)
                        .addHeader(XOSNAME,XOSNAME_VAL)
                        .addHeader("Accept-Language","en-SG")
                        .addHeader("X-Site-Country","sg")
                        .addHeader("X-App-Platform","mobileapp_ios");
                return chain.proceed(builder.build());
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIInterface apiInterface = retrofit.create(APIInterface.class);

        try {
            Response<JsonObject> response = apiInterface.getProductInfo(params).execute();
            Assert.assertTrue(response.isSuccessful());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
