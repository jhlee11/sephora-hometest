package com.demo.sephora_demo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductModel implements Parcelable {

    private String id;
    private String name;
    private String price;
    private String rating;
    private String imgUrl;
    private String description;
    private String benefits;
    @SerializedName("how-to-text")
    private String howTo;
    private String ingredients;
    @SerializedName("reviews-count")
    private String reviewsCount;
    @SerializedName("default-image-urls")
    private List<String> defaultImgUrl;
    private String brand;

    public ProductModel() {

    }

    protected ProductModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        price = in.readString();
        rating = in.readString();
        imgUrl = in.readString();
        description = in.readString();
        benefits = in.readString();
        howTo = in.readString();
        ingredients = in.readString();
        reviewsCount = in.readString();
        defaultImgUrl = in.createStringArrayList();
        brand = in.readString();
    }

    public static final Creator<ProductModel> CREATOR = new Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(price);
        dest.writeString(rating);
        dest.writeString(imgUrl);
        dest.writeString(description);
        dest.writeString(benefits);
        dest.writeString(howTo);
        dest.writeString(ingredients);
        dest.writeString(reviewsCount);
        dest.writeStringList(defaultImgUrl);
        dest.writeString(brand);
    }

    public List<String> getDefaultImgUrl() {
        return defaultImgUrl;
    }

    public void setDefaultImgUrl(List<String> defaultImgUrl) {
        this.defaultImgUrl = defaultImgUrl;
    }

    public String getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(String reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBenefits() {
        return benefits;
    }

    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }

    public String getHowTo() {
        return howTo;
    }

    public void setHowTo(String howTo) {
        this.howTo = howTo;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

}
