package com.demo.sephora_demo.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Map;

public class DataModel implements Parcelable {
    private String type;
    private String id;
    private ProductModel attributes;
    private Map<String, Object> relationships;
    private Map<String, Object> brandMap;
    private Map<String, Object> dataMap;
    private String brand_id;

    protected DataModel(Parcel in) {
        type = in.readString();
        id = in.readString();
        brand_id = in.readString();
        brand = in.readString();
    }

    public static final Creator<DataModel> CREATOR = new Creator<DataModel>() {
        @Override
        public DataModel createFromParcel(Parcel in) {
            return new DataModel(in);
        }

        @Override
        public DataModel[] newArray(int size) {
            return new DataModel[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(id);
        dest.writeString(brand_id);
        dest.writeString(brand);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    private String brand;

    public Map<String, Object> getRelationships() {
        return relationships;
    }

    public void setRelationships(Map<String, Object> relationships) {
        this.relationships = relationships;
    }

    public String getBrand_id() {
        brandMap = (Map<String, Object>) relationships.get("brand");
        dataMap = (Map<String, Object>) brandMap.get("data");
        return dataMap.get("id").toString();
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public ProductModel getAttributes() {
        return attributes;
    }

    public void setAttributes(ProductModel attributes) {
        this.attributes = attributes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
