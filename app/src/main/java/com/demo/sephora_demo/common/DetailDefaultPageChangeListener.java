package com.demo.sephora_demo.common;

import android.util.Log;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;


public class DetailDefaultPageChangeListener implements ViewPager.OnPageChangeListener {
    private final ViewPager viewPager;
    private int mPageToChange = 0;

    public DetailDefaultPageChangeListener(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    /**
     * Detail Image viewPager Selected Page image showing
     * @param position
     */
    @Override
    public void onPageSelected(int position) {
        PagerAdapter adapter = viewPager.getAdapter();
        int size = adapter.getCount() - 2;
        int pageIndex = position;

        if (position == 0) {
            pageIndex = size;
        } else if (position == size + 1) {
            pageIndex = 1;
        } else if (position >= 1 && position <= size) {
            int index = position - 1;
        }
        if (position != pageIndex) {
            mPageToChange = pageIndex;
            return;
        }
    }

    /**
     * Detail Image ViewPager Scroll State
     * 0 : Scroll End
     * 1 : Drag Start
     * 2 : Drag End
     * @param state
     */
    @Override
    public void onPageScrollStateChanged(int state) {
        switch (state) {
            case ViewPager.SCROLL_STATE_IDLE:
                if (mPageToChange != 0) {
                    viewPager.setCurrentItem(mPageToChange, false);
                    mPageToChange = 0;
                }
                break;

            case ViewPager.SCROLL_STATE_DRAGGING:
                break;

            case ViewPager.SCROLL_STATE_SETTLING:
                break;

            default:
                break;
        }
    }
}
