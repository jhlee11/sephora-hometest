package com.demo.sephora_demo.common;

public class Constant {
    /**
     * http header key
     */
    public static final String ACCEPT = "Accept";
    public static final String XDMODEL = "X-Device-Model";
    public static final String XPLATFORM = "X-Platform";
    public static final String USERAGENT = "User-Agent";
    public static final String XAVERSION = "X-App-Version";
    public static final String XOSNAME = "X-OS-Name";

    /**
     * http header value
     */
    public static final String ACCEPT_VAL = "application/json";
    public static final String XDMODEL_VAL = "iphone";
    public static final String XPLATFORM_VAL = "mobile_app";
    public static final String USERAGENT_VAL = "Sephora/2.0.1 (com.sephora.digital; build:7; iOS 13.2.3) Alamofire/4.7.3";
    public static final String XAVERSION_VAL = "2.0.1";
    public static final String XOSNAME_VAL = "ios";

    /**
     * ProductList Parameter key
     */
    public static final String paramKey1 = "filter[platform]";
    public static final String paramKey2 = "filter[product_group]";
    public static final String paramKey3 = "include";
    public static final String paramKey4 = "page[size]";

    /**
     * ProductList Parameter value
     */
    public static final String paramVal1 = "mobile_app";
    public static final String paramVal2 = "bestsellers";
    public static final String paramVal3 = "brand";
    public static final String paramVal4 = "30";

    /**
     * response JSON Key
     */
    public static final String META = "meta";
    public static final String TOTAL = "total";
    public static final String LINKS = "links";
    public static final String NEXT = "next";
    public static final String INCLUDED = "included";
    public static final String DATA = "data";
    public static final String DETAIL = "detail";
    public static final String ATTRIBUTES = "attributes";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String RATING = "rating";
    public static final String DESCRIPTION = "description";
    public static final String BENEFITS = "benefits";
    public static final String pHOWTO = "how-to";
    public static final String pINGREDIENTS = "ingredients";
    public static final String REVIEWCNT = "reviews-count";
    public static final String DEFAULTIIMG= "default-image-urls";
    public static final String CLOSEUPIIMG= "closeup-image-urls";

    public static final String ID = "id";
    public static final String BRAND = "brand";
    public static final String RELATIONSHIPS = "relationships";



    /**
     * Remaining characters
     */
    public static final String itemFound = " items Found";
    public static final String HOWTO = "HOW TO";
    public static final String INGREDIENTS = "INGREDIENTS";
    public static final String LESS = "Less";
    public static final String MORE = "More";
    public static final int SHOW = 0;
    public static final int HIDE = 1;
    public static final String REVIEWS = " Reviews";
}
