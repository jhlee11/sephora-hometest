package com.demo.sephora_demo.common;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;

import com.demo.sephora_demo.R;

public class Util {
    /**
     * Set TextView UnderLine
     * @param msg
     * @return SpannableString
     */
    public static SpannableString setUnderLine(String msg){
        SpannableString content = new SpannableString(msg);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        return content;
    }

    /**
     * Set Drawable Image
     * @param imgView
     * @param context
     * @param type
     * @return ImageView
     */
    public static ImageView setImage(ImageView imgView, Context context, String type){
        int id = 0;
        switch (type) {
            case "starFull":
                id = R.drawable.star_red;
                break;
            case "starHalf":
                id = R.drawable.star_half;
                break;
            case "starOff":
                id = R.drawable.star_gray;
                break;
            case "circleRed":
                id = R.drawable.circle_red;
                break;
            case "circleGray":
                id = R.drawable.circle_gray;
                break;
            default:
                imgView.setVisibility(View.GONE);
                return imgView;
        }
        imgView.setImageDrawable(context.getResources().getDrawable(id));
        return imgView;
    }
}
