package com.demo.sephora_demo.common;

public class NetInfo {
    /**
     * URL
     */
    public static final String BASE_URL = "https://api.sephora.sg";
    public static final String API_URL = "/api/v2.5/products";
}
