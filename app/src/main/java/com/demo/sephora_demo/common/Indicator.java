package com.demo.sephora_demo.common;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.demo.sephora_demo.R;

/**
 * Created by jhlee on 2019. 12. 12.
 * Copyright (c) 2019년 Spree. All rights reserved.
 */

/**
 * Set Indicator for Custom
 */
public class Indicator implements OnTouchListener {
    private Activity xActivity = null;
    private View xView = null;
    private View xViewOverActivity = null;

    private boolean isFirst = true;
    private boolean isShow = false;

    private final int ACTIVITY = 1;
    private final int HIDE = 2;

    private int iMode = 0;

    public Indicator(Activity activity) {
        xViewOverActivity = View.inflate(activity, R.layout.dialog_indicator, null);
        xActivity = activity;
        iMode = ACTIVITY;
        xViewOverActivity.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
    }

    public void show() {
        if (!isShow) {
            isShow = true;
            Method(iMode);
        }
    }

    public void hide() {
        Method(HIDE);
    }

    public boolean isShowing() {
        return isShow;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return true;
    }

    void Method(int mode) {
        switch (mode) {
            case ACTIVITY: {
                xViewOverActivity.setFocusable(true);

                if (isFirst) {
                    xActivity.addContentView(xViewOverActivity, new LayoutParams(
                            LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
                    isFirst = false;
                } else {
                    xViewOverActivity.setVisibility(View.VISIBLE);
                }

                break;
            }

            case HIDE: {
                xViewOverActivity.setVisibility(View.GONE);
                xViewOverActivity.setFocusable(false);
                if (xView != null) {
                    xView.setVisibility(View.GONE);
                }
                isShow = false;

                break;
            }
        }
    }
}
