package com.demo.sephora_demo.di.component;

import com.demo.sephora_demo.activity.ProductListActivity;
import com.demo.sephora_demo.di.module.NetModule;

import dagger.Component;

@Component(modules = NetModule.class)
public interface NetComponent {

    /**
     * Inject NetModule class to ProductListActivity
     * @param activity
     */
    void inject(ProductListActivity activity);
}
