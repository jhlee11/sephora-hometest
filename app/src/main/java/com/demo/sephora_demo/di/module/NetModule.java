package com.demo.sephora_demo.di.module;

import android.util.Log;

import java.io.IOException;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.demo.sephora_demo.common.Constant.*;
import static com.demo.sephora_demo.common.NetInfo.*;

@Module
public class NetModule {

    /**
     * Http client to apply headers
     * @return client
     */
    @Provides
    public OkHttpClient okHttpClient() {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder()
                        .addHeader(ACCEPT,ACCEPT_VAL)
                        .addHeader(XDMODEL,XDMODEL_VAL)
                        .addHeader(XPLATFORM,XPLATFORM_VAL)
                        .addHeader(USERAGENT,USERAGENT_VAL)
                        .addHeader(XAVERSION,XAVERSION_VAL)
                        .addHeader(XOSNAME,XOSNAME_VAL)
                        .addHeader("Accept-Language","en-SG")
                        .addHeader("X-Site-Country","sg")
                        .addHeader("X-App-Platform","mobileapp_ios");
                return chain.proceed(builder.build());
            }
        }).addInterceptor(httpLoggingInterceptor()).build();

        return client;
    }

    /**
     * Baseurl and GsonConverter Settings
     * @param httpClient
     * @return retrofit
     */
    @Provides
    public Retrofit retrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Interceptor method for Network logging
     * @return interceptor
     */
    @Provides
    public HttpLoggingInterceptor httpLoggingInterceptor(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("okhttp", message);
            }
        });

        return interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    }
}
