package com.demo.sephora_demo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demo.sephora_demo.R;
import com.demo.sephora_demo.activity.ProductDetailActivity;
import com.demo.sephora_demo.model.BrandModel;
import com.demo.sephora_demo.model.DataModel;
import com.demo.sephora_demo.model.ProductModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.demo.sephora_demo.common.Constant.*;
import static com.demo.sephora_demo.common.Util.setImage;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {
    private Context mContext;

    // 2020.03.04 Change Model Set
    private List<DataModel> dataModel;
    private List<BrandModel> brandModel;

    public ProductListAdapter(Context mContext) {
        this.mContext = mContext;
        this.dataModel = new LinkedList<DataModel>();
        this.brandModel = new LinkedList<BrandModel>();
    }

    /**
     * Add Product Model
     * @param arr
     */
    public void addProductList(JsonArray arr){
        Gson gson = new Gson();
        DataModel[] list = gson.fromJson(arr.toString(), DataModel[].class);
        this.dataModel.addAll(Arrays.asList(list));

        notifyDataSetChanged();
    }

    /**
     * Add Brand Model
     * @param arr
     */
    public void addBrandList(JsonArray arr){
        Gson gson = new Gson();
        BrandModel[] list = gson.fromJson(arr.toString(), BrandModel[].class);
        this.brandModel.addAll(Arrays.asList(list));

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_list, parent, false));
    }

    /**
     * set Product item Information
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        for(int j=0; j<this.brandModel.size(); j++) {
            if(this.brandModel.get(j).getId().equals(dataModel.get(position).getBrand_id())){
                dataModel.get(position).getAttributes().setBrand(this.brandModel.get(j).getName());
            }
        }
        holder.render(dataModel.get(position).getAttributes());
    }

    @Override
    public int getItemCount() {
        return dataModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        @BindView(R.id.item_layout)
        LinearLayout layoutPanel;
        @BindView(R.id.item_layout_image)
        RelativeLayout layoutImage;
        @BindView(R.id.item_image)
        ImageView imgThumb;
        @BindView(R.id.item_brand_name)
        TextView txtBrand;
        @BindView(R.id.item_product_name)
        TextView txtProductName;
        @BindView(R.id.item_product_price)
        TextView txtPrice;
        @BindViews({R.id.img_star_1,R.id.img_star_2,R.id.img_star_3,R.id.img_star_4,R.id.img_star_5})
        ImageView[] imgStar;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void render(ProductModel model){
            ViewGroup.LayoutParams params = layoutImage.getLayoutParams();
            int width = params.width;
            params.height = width;
            layoutImage.setLayoutParams(params);
            DecimalFormat format = new DecimalFormat("###,###");

            String name = model.getName();
            String price = Currency.getInstance(Locale.KOREA).getSymbol() + format.format(Long.parseLong(model.getPrice()));
            String imageUrl = model.getDefaultImgUrl().get(0);
            float rating = Float.valueOf(model.getRating());

            Glide.with(mContext)
                    .load(imageUrl)
                    .placeholder(R.drawable.thumb_loading_icon)
                    .error(R.drawable.thumb_loading_icon)
                    .into(imgThumb);

            txtBrand.setText(model.getBrand());
            txtProductName.setText(name);
            txtPrice.setText(price);

            // Set Rating of Star
            for (int j=0; j<imgStar.length; j++) {
                if (rating >= (float) j + 1) {
                    // full
                    setImage(imgStar[j],mContext,"starFull");
                } else if (rating > (float) j) {
                    // half
                    setImage(imgStar[j],mContext,"starHalf");
                } else {
                    // off
                    setImage(imgStar[j],mContext,"starOff");
                }
            }

            layoutPanel.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, ProductDetailActivity.class);
            intent.putExtra(DETAIL, dataModel.get(getAdapterPosition()).getAttributes());
            mContext.startActivity(intent);
        }
    }
}
