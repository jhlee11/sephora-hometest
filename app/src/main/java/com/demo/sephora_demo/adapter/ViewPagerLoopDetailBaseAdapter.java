package com.demo.sephora_demo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.demo.sephora_demo.R;

import java.util.ArrayList;
import java.util.List;


public class ViewPagerLoopDetailBaseAdapter extends PagerAdapter {

    protected ArrayList<View> viewList;

    @Override
    public int getCount() {
        return viewList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        if ((viewList != null) && (viewList.size() > 0)) {
            final View view = viewList.get(position);
            container.post(new Runnable() {
                @Override
                public void run() {
                    container.addView(view);
                }
            });
            return view;
        }
        return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(final ViewGroup container, int position, Object object) {
        if (viewList != null) {
            try{
                final View view = viewList.get(position);
                container.post(new Runnable() {
                    @Override
                    public void run() {
                        container.removeView(view);
                    }
                });
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Infinite Loop Process
     * @param dataList
     * @param context
     * @param onClickListener
     */
    public void setDataList(final List<String> dataList, Context context, View.OnClickListener onClickListener) {
        if (dataList != null) {
            int size = dataList.size();
            int length;
            if (size > 1) {
                length = size + 2;
            } else {
                length = size;
            }
            ArrayList<View> viewList = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                if (i == 0) {
                    int index = size - 1;
                    View view = getImageView(dataList, context, index, onClickListener);
                    viewList.add(view);

                } else if (i == (length - 1)) {
                    int index = 0;
                    View view = getImageView(dataList, context, index, onClickListener);
                    viewList.add(view);
                } else if (i > 0 && i < (length - 1)) {
                    int index = i - 1;
                    View view = getImageView(dataList, context, index, onClickListener);
                    viewList.add(view);
                }
            }
            this.viewList = viewList;
        }
    }

    /**
     * Product Detail Set Image
     * @param dataList
     * @param context
     * @param index
     * @param onClickListener
     * @return view
     */
    public View getImageView(List<String> dataList, Context context, int index, View.OnClickListener onClickListener) {
        String url = dataList.get(index);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.page_product_detail, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.img_detail_page);
        imageView.setOnClickListener(onClickListener);
        Glide.with(context.getApplicationContext()).load(url).placeholder(R.drawable.thumb_loading_icon).error(R.drawable.thumb_loading_icon).into(imageView);
        return view;
    }
}
