package com.demo.sephora_demo.adapter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;
import com.demo.sephora_demo.common.DetailDefaultPageChangeListener;

/**
 * Detail Image Infinite viewPager
 */
public class DetailInfiniteLoopViewPager extends ViewPager {
    private final Context mContext;
    private final GestureDetector mGestureDetector;

    public DetailInfiniteLoopViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        mGestureDetector = new GestureDetector(getContext(), new DetailInfiniteLoopViewPager.HorizontalMotionDetector());
        this.mContext = context;
    }

    public void setAdapter(ViewPagerLoopDetailBaseAdapter adapter) {
        super.setAdapter(adapter);
        this.setCurrentItem(0);
        DetailDefaultPageChangeListener defaultPageChangeListener = new DetailDefaultPageChangeListener(this);
        this.addOnPageChangeListener(defaultPageChangeListener);
    }

    /**
     * Set Current item Information
     * @param item
     */
    @Override
    public void setCurrentItem(int item) {
        if(item < 1){
            super.setCurrentItem(1);
        }
        else{
            super.setCurrentItem(item);
        }
    }

    /**
     * Detect Horizontal Scroll Action
     */
    private class HorizontalMotionDetector implements GestureDetector.OnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {}

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return Math.abs(distanceX) * 3 > Math.abs(distanceY);
        }

        @Override
        public void onLongPress(MotionEvent e) {}

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }
    }
}
