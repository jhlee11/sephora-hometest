package com.demo.sephora_demo.adapter;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.demo.sephora_demo.fragment.ProductDetailTabFragment;
import com.demo.sephora_demo.model.ProductModel;

import static com.demo.sephora_demo.common.Constant.*;

public class ProductDetailTabAdapter extends FragmentStatePagerAdapter {

    private int tabCount;
    private ProductModel productModel;

    public ProductDetailTabAdapter(FragmentManager fm, int tabCount, ProductModel productModel) {
        super(fm);
        this.tabCount = tabCount;
        this.productModel = productModel;
    }

    /**
     * Set Tab Fragment HOW TO / INGREDIENTS
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new ProductDetailTabFragment();
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(DETAIL,this.productModel);
        switch (position){
            case 0:
                bundle.putSerializable("type","howTo");
                fragment.setArguments(bundle);
                return fragment;
            case 1:
                bundle.putSerializable("type","ingredients");
                fragment.setArguments(bundle);
                return fragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
