package com.demo.sephora_demo.fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demo.sephora_demo.R;
import com.demo.sephora_demo.model.ProductModel;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.demo.sephora_demo.common.Constant.*;
import static com.demo.sephora_demo.common.Util.setUnderLine;

public class ProductDetailTabFragment extends Fragment {

    @BindView(R.id.txt_content)
    TextView txtContent;
    @BindView(R.id.txt_more)
    TextView txtMore;

    private ProductModel productModel = new ProductModel();
    private boolean more = true;
    private String contents;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Setting the content according to the argument
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_howto, null);

        ButterKnife.bind(this, view);

        productModel = (ProductModel) getArguments().getParcelable(DETAIL);

        if(getArguments().getSerializable("type").equals("howTo")){
            contents = productModel.getHowTo().replace("\\r\\n","<br>");
        }else{
            contents = productModel.getIngredients().replace("\\r\\n","<br>");
        }

        txtContent.setText(Html.fromHtml(contents));
        txtMore.setText(setUnderLine(MORE));
        txtMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(more){
                    txtContent.setMaxLines(50);
                    txtMore.setText(setUnderLine(LESS));
                    more = false;
                }else{
                    txtContent.setMaxLines(4);
                    txtMore.setText(setUnderLine(MORE));
                    more = true;
                }
            }
        });

        return view;
    }
}
