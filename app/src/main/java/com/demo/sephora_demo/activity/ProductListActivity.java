package com.demo.sephora_demo.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.sephora_demo.R;
import com.demo.sephora_demo.adapter.ProductListAdapter;
import com.demo.sephora_demo.api.APIInterface;
import com.demo.sephora_demo.common.Indicator;
import com.demo.sephora_demo.di.component.DaggerNetComponent;
import com.demo.sephora_demo.di.component.NetComponent;
import com.demo.sephora_demo.di.module.NetModule;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.demo.sephora_demo.common.Constant.*;

public class ProductListActivity extends Activity {

    private Context mContext;
    private ProductListAdapter productListAdapter;
    private Indicator indicator;
    private String nextUrl;
    private int totalPages = 1;
    private int currentPage = 1;
    private boolean isLoad = false;
    private APIInterface apiInterface;

    @BindView(R.id.view_product_list)
    RecyclerView recyclerView;
    @BindView(R.id.product_list_count)
    TextView productListCount;

    @Inject
    Retrofit retrofit;

    /**
     * Indicator handler
     */
    protected Handler handlerIndicator = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (!indicator.isShowing())
                        indicator.show();
                    break;
                case 1:
                    if (indicator.isShowing())
                        indicator.hide();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        ButterKnife.bind(this);

        indicator = new Indicator(this);
        mContext = this;
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        productListAdapter = new ProductListAdapter(mContext);

        NetComponent netComponent = DaggerNetComponent.builder()
                .netModule(new NetModule())
                .build();
        netComponent.inject(this);
        apiInterface = retrofit.create(APIInterface.class);
        getAPI(true, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Get Product List
     * Call api every 2 seconds when scroll down
     *
     * @param first : If first set BrandList Model
     * @param url : Next URL Call
     */
    private void getAPI(final Boolean first, final String url){
        if(first){
            recyclerView.setHasFixedSize(true);
            handlerIndicator.obtainMessage(SHOW).sendToTarget();
            Map<String, Object> params = new HashMap<>();
            params.put(paramKey1,paramVal1);
            params.put(paramKey2,paramVal2);
            params.put(paramKey3,paramVal3);
            params.put(paramKey4,paramVal4);

            apiInterface.getProductInfo(params).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    handlerIndicator.obtainMessage(HIDE).sendToTarget();
                    JsonObject meta = response.body().getAsJsonObject(META);
                    totalPages = response.body().getAsJsonObject(META).get("total-pages").getAsInt();
                    currentPage = response.body().getAsJsonObject(META).get("current-page").getAsInt();

                    productListCount.setText(meta.get(TOTAL) + itemFound);
                    if(!response.body().getAsJsonObject(LINKS).has(NEXT)){
                        nextUrl = response.body().getAsJsonObject(LINKS).get(NEXT) + "";
                    }else{
                        nextUrl = "";
                    }

                    productListAdapter.addBrandList(response.body().getAsJsonArray(INCLUDED));
                    productListAdapter.addProductList(response.body().getAsJsonArray(DATA));
                    recyclerView.setAdapter(productListAdapter);
                    recyclerView.requestLayout();
                    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            if(dy > 0){
                                if(!nextUrl.equals("")){
                                    if(totalPages == currentPage){
                                        recyclerView.removeOnScrollListener(this);
                                    }
                                    if(totalPages>currentPage && !isLoad){
                                        isLoad = true;
                                        getAPI(false, nextUrl.replace("\"", ""));
                                    }
                                }
                            }
                        }
                    });
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d("error",t.getMessage());
                }
            });
        }else{
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                isLoad = false;
            },2000);
            apiInterface.getProductInfoPaging(url).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if(!response.body().get(META).isJsonNull()){
                        JsonObject meta = response.body().getAsJsonObject(META);
                        currentPage = response.body().getAsJsonObject(META).get("current-page").getAsInt();
                        productListCount.setText(meta.get(TOTAL) + itemFound);
                        if(response.body().getAsJsonObject(LINKS).has(NEXT)){
                            nextUrl = response.body().getAsJsonObject(LINKS).get(NEXT) + "";
                        }else{
                            nextUrl = "";
                        }
                        productListAdapter.addBrandList(response.body().getAsJsonArray(INCLUDED));
                        productListAdapter.addProductList(response.body().getAsJsonArray(DATA));
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d("error",t.getMessage());
                }
            });
        }
    }
}
