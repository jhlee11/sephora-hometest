package com.demo.sephora_demo.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.demo.sephora_demo.R;
import com.demo.sephora_demo.adapter.DetailInfiniteLoopViewPager;
import com.demo.sephora_demo.adapter.ProductDetailTabAdapter;
import com.demo.sephora_demo.adapter.ViewPagerLoopDetailBaseAdapter;
import com.demo.sephora_demo.model.ProductModel;
import com.google.android.material.tabs.TabLayout;

import java.text.DecimalFormat;
import java.util.Currency;
import java.util.Locale;

import static com.demo.sephora_demo.common.Constant.*;
import static com.demo.sephora_demo.common.Util.setImage;
import static com.demo.sephora_demo.common.Util.setUnderLine;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductDetailActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.layout_reviews)
    LinearLayout layoutReviews;
    @BindView(R.id.layout_img_circle)
    LinearLayout layoutImgCircle;

    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_brand)
    TextView txtBrand;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_price)
    TextView txtPrice;
    @BindView(R.id.txt_reviewsCount)
    TextView txtReviewsCount;
    @BindView(R.id.txt_more)
    TextView txtMore;
    @BindView(R.id.txt_description)
    TextView txtDescription;
    @BindView(R.id.view_benefits)
    TextView viewBenefits;
    @BindView(R.id.txt_benefits)
    TextView txtBenefits;
    @BindView(R.id.btn_back)
    Button btnBack;
    @BindViews({R.id.img_star_1,R.id.img_star_2,R.id.img_star_3,R.id.img_star_4,R.id.img_star_5})
    ImageView[] imgStar = new ImageView[5];
    @BindView(R.id.viewPager)
    DetailInfiniteLoopViewPager viewPager;
    @BindView(R.id.layout_tab)
    TabLayout layoutTab;
    @BindView(R.id.viewPagerTab)
    ViewPager viewPagerTab;

    ProductModel productModel = new ProductModel();
    ImageView[] iv;

    private Boolean more = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);

        DecimalFormat format = new DecimalFormat("###,###");

        productModel = (ProductModel) getIntent().getParcelableExtra(DETAIL);

        txtTitle.setText(productModel.getBrand());
        txtBrand.setText(productModel.getBrand());
        txtName.setText(productModel.getName());
        txtPrice.setText(Currency.getInstance(Locale.KOREA).getSymbol() + format.format(Long.parseLong(productModel.getPrice())));
        txtDescription.setText(Html.fromHtml(productModel.getDescription()));
        txtBenefits.setText(Html.fromHtml(productModel.getBenefits()));
        txtMore.setText(setUnderLine(MORE));

        float rating = Float.valueOf(productModel.getRating());

        for (int j=0; j<imgStar.length; j++) {
            if (rating >= (float) j + 1) {
                // full
                setImage(imgStar[j],this,"starFull");
            } else if (rating > (float) j) {
                // half
                setImage(imgStar[j],this,"starHalf");
            } else {
                // off
                setImage(imgStar[j],this,"starOff");
            }
        }

        txtReviewsCount.setText(setUnderLine(productModel.getReviewsCount() + REVIEWS));
        if(productModel.getReviewsCount().equals("0") || productModel.getReviewsCount().equals("")){
            layoutReviews.setVisibility(View.GONE);
        }

        LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(25,25);
        layoutparams.setMargins(10,10,10,10);

        iv = new ImageView[productModel.getDefaultImgUrl().size()];
        for(int i=0; i<productModel.getDefaultImgUrl().size(); i++){

            iv[i] = new ImageView(this);
            iv[i].setLayoutParams(layoutparams);
            if(i == 0){
                iv[i].setImageResource(R.drawable.circle_red);
            }else{
                iv[i].setImageResource(R.drawable.circle_gray);
            }
            layoutImgCircle.addView(iv[i]);
        }

        // Image Viewpager layout ratio 1:1
        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.height = params.width;

        ViewPagerLoopDetailBaseAdapter mAdapter = new ViewPagerLoopDetailBaseAdapter();
        mAdapter.setDataList(productModel.getDefaultImgUrl(), this, null);

        viewPager.setLayoutParams(params);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(this);

        layoutTab.addTab(layoutTab.newTab().setText(HOWTO));
        layoutTab.addTab(layoutTab.newTab().setText(INGREDIENTS));
        layoutTab.setTabGravity(TabLayout.GRAVITY_FILL);

        ProductDetailTabAdapter adapter = new ProductDetailTabAdapter(getSupportFragmentManager(), layoutTab.getTabCount(), productModel);
        viewPagerTab.setAdapter(adapter);
        viewPagerTab.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(layoutTab));

        layoutTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerTab.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @OnClick(R.id.btn_back)
    public void onClick(View v) {
        finish();
    }

    @OnClick(R.id.txt_more)
    public void changeMore(){
        if(more){
            txtDescription.setMaxLines(50);
            viewBenefits.setVisibility(View.VISIBLE);
            txtBenefits.setVisibility(View.VISIBLE);
            txtMore.setText(setUnderLine(LESS));
            more = false;
        }else{
            txtDescription.setMaxLines(1);
            viewBenefits.setVisibility(View.GONE);
            txtBenefits.setVisibility(View.GONE);
            txtMore.setText(setUnderLine(MORE));
            more = true;
        }
    }

    /**
     * Dot representation by number of images
     * @param position
     * @param positionOffset
     * @param positionOffsetPixels
     */
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(position > 0){
            for(int i=0; i<iv.length; i++){
                if(i == (position-1)){
                    setImage(iv[i],this,"circleRed");
                }else{
                    setImage(iv[i],this,"circleGray");
                }
            }
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
