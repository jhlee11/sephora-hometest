package com.demo.sephora_demo.api;

import com.demo.sephora_demo.common.NetInfo;
import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Retrofit2 API Interface
 */
public interface APIInterface {

    @GET(NetInfo.API_URL)
    Call<JsonObject> getProductInfo(@QueryMap(encoded = true) Map<String, Object> params);

    @GET
    Call<JsonObject> getProductInfoPaging(@Url String url);


}
